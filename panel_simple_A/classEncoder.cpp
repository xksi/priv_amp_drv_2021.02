﻿#define ENCODER_PORT_PIN_A   PIND
#define ENCODER_PORT_PIN_B   PIND
#define ENCODER_PORT_BTN     PINB
#define ENCODER_PIN_A           4 // ( PORTD.2 )
#define ENCODER_PIN_B           8 // ( PORTD.3 )
#define ENCODER_BTN             4 // ( PORTB.2 )

class classEncoder{
  
  public:

    volatile int8_t minValue ;
    volatile int8_t maxValue ; 
    volatile int8_t currentValue ;

    void init( int8_t minValue, int8_t maxValue, int8_t currentValue ){

      this->minValue = minValue ;
      this->maxValue = maxValue ;
      this->currentValue = currentValue ;

      //
	    // konfiguracja przerwań obsługi enkodera.
      //
      GIMSK |= (1<<INT0);     // włączenie przerwania INT0
      GIMSK |= (1<<INT1);     // włączenie przerwania INT1
      MCUCR = ( MCUCR | 14 ); // 0 opadające, 1 narastające
    }

    void pinBintFunction(){
      if( ( ENCODER_PIN_B & ENCODER_PORT_PIN_B ) == 0 ) {
        if ( this->currentValue < this->maxValue ) this->currentValue++;
      } else {
        if ( this->currentValue > this->minValue ) this->currentValue--;
      }
    }

    void pinAintFunction(){
      if( ( ENCODER_PIN_A & ENCODER_PORT_PIN_A ) == 0 ) {
        if ( this->currentValue < this->maxValue ) this->currentValue++;
      } else {
        if ( this->currentValue > this->minValue ) this->currentValue--;
      }
    }

    bool isButtonPressed(){
      return !( ( ENCODER_PORT_BTN & ENCODER_BTN ) == ENCODER_BTN ) ;
    }

};

