﻿
class classTools{
  
  public:
  
    int8_t numberAbs( int8_t number ){
      if ( number < 0) return ( number * -1 ) ;
      return number ;
    }
    
    int8_t numberSign( int8_t number ){
      if ( number > 0) return 1;
      if ( number < 0) return -1;
      return 0 ;
    }
    
    uint8_t toHex( uint8_t hx ) {
      if ( ( hx >=  0 ) && ( hx <=  9 ) ) return '0' + hx ;
      if ( ( hx >= 10 ) && ( hx <= 15 ) ) return ( 'A' - 10 ) + hx ;
      return 0 ;
    }

    uint8_t toByte( uint8_t hx ) {
      if ( hx >= 'A')
      return hx - 'A' + 10;
      else
      return hx - '0';
    }
    
    uint16_t floorSqrt( uint16_t x ) {
      if ( x == 0 || x == 1 ) return x;  
      uint16_t i = 1, result = 1 ;
      while ( result <= x ){
        i++;
        result = i * i;
      }
      return i - 1;
    } 
    
    void delay ( uint16_t iterations ){
      uint16_t i ;
      for ( i = 0 ; i <= iterations ; i++  ){
        _delay_ms(1);
      }
      return;
    }
    
};

