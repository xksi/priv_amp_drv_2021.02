﻿#include <util/delay.h>
#define TLC5925_PORT_NOTOE        PORTB
#define TLC5925_PIN_NOTOE_VALUE   bit_3_value
#define TLC5925_PORT_LE           PORTB
#define TLC5925_PIN_LE_VALUE      bit_1_value
#define TLC5925_PORT_CLK          PORTB
#define TLC5925_PIN_CLK_VALUE     bit_0_value
#define TLC5925_PORT_SDI          PORTD
#define TLC5925_PIN_SDI_VALUE     bit_6_value
#define TLC5925_DEF_DELAY         1

class classTLC5925{
    
  public:      
  
    static const uint8_t LED_STATE_ON = 1 ;
    static const uint8_t LED_STATE_OFF = 0 ;
    static const uint8_t LED_STATE_INVERSE = 2 ;
    static const uint8_t LAST_LED_NO = 15;
    static const uint8_t MID_LED_NO = 8;
  
    uint8_t ledStates [ 16 ]; // array represents LED states

  void shortDemo( uint8_t repeats ){
    uint8_t i = 0 ;
    while ( i <= (((this->LAST_LED_NO+1)*2)-1 )*repeats ) {
      this->ledStates [ i % ( this->LAST_LED_NO + 1 ) ] = 1-this->ledStates [ i % ( this->LAST_LED_NO + 1 ) ] ;
      _delay_ms( 20 );
      this->send();
      i++;
    }
  }    
      
  void init(){ 
    //
    // little demo on init
    uint8_t i;
    for ( i = 0 ; i <= this->LAST_LED_NO ; i++ ){
      this->ledStates [ i ] = 0 ;
    }
  }
  
  void set ( uint8_t no, uint8_t state ) {
    if ( state == this->LED_STATE_INVERSE ) this->ledStates [ no ] = 1 - this->ledStates [ no ] ;
    if ( state == this->LED_STATE_ON ) this->ledStates [ no ] = this->LED_STATE_ON ;
    if ( state == this->LED_STATE_OFF ) this->ledStates [ no ] = this->LED_STATE_OFF ;
    this->send() ;
  }
  
  void setRange ( uint8_t from, uint8_t to, uint8_t state ){
    uint8_t i ;
    if ( from > to ) {
      i = to ; to = from ;  from = i ;  // swap
    }
    for ( i = from ; i <= to ; i++ ){
      if ( state == this->LED_STATE_INVERSE ) this->ledStates [ i ] = 1 - this->ledStates [ i ] ;
      if ( state == this->LED_STATE_ON ) this->ledStates [ i ] = this->LED_STATE_ON ;
      if ( state == this->LED_STATE_OFF ) this->ledStates [ i ] = this->LED_STATE_OFF ;
    }
    this->send();    
  }  
  
  void setAll( uint8_t state ){
    this->setRange ( 0, this->LAST_LED_NO, state );
    return;
  }
  
  void setAllOff(){
    this->setAll ( this->LED_STATE_OFF );
    return;
  }
  
  void showNiceLinear( int8_t value, int8_t max ){    
    int16_t ratio = ( ( this->LAST_LED_NO ) * 256 ) / max ;
    int16_t length = ( value * ratio ) / 256 ;
    this->setAllOff();
    if ( value == 0 ) return;
    this->setRange( 0, length, this->LED_STATE_ON ) ;
    if ( value >= DigiPot.MAX_ATTENTUATION - 1 ) this->setAll( this->LED_STATE_ON ) ;
    return;
  }
  
  void showNiceExp( uint8_t value ){

    int32_t maxVal = DigiPot.MUTE_ATTENTUATION ;
    int32_t vol = value ;
    int32_t dispValue = ( 65536 / ( maxVal ) ) * ( vol * vol )  ;
    if ( dispValue < 1 ) dispValue = 1 ;
    dispValue = dispValue / 65536 ;
    if ( ( value > 0 )&&( dispValue == 0 ) ) dispValue = 1 ;
    if ( value == 0 ) dispValue = 0 ;
    this->showNiceLinear( dispValue, DigiPot.MUTE_ATTENTUATION );
  }
  
  int8_t niceConversionArray( int8_t x ){
    int8_t y = 0 ;
    int8_t sign = Tools.numberSign ( x );
    x = Tools.numberAbs ( x );    
    if ( x >=  1 ) y = 1 ;
    if ( x >=  4 ) y = 2 ;
    if ( x >=  9 ) y = 3 ;
    if ( x >= 18 ) y = 4 ;
    if ( x >= 35 ) y = 5 ;
    if ( x >= 60 ) y = 6 ;
    if ( x >= 84 ) y = 7 ;
    return ( y * sign ) ;
  }
  
  void showNiceBalance( int16_t value ){    
    this->setAllOff();
    this->setRange( this->MID_LED_NO, this->MID_LED_NO + this->niceConversionArray( value ), this->LED_STATE_ON ) ;
    return;
  }  
  
  void send(){    
    // sends led states to TLC
    uint8_t i ;
    TLC5925_PORT_CLK = TLC5925_PORT_CLK & ( 255 - TLC5925_PIN_SDI_VALUE ) ;       // set CLK = 0
    _delay_us( TLC5925_DEF_DELAY );                                               // delay
    TLC5925_PORT_LE = TLC5925_PORT_LE & ( 255 - TLC5925_PIN_LE_VALUE ) ;          // set LE = 0
    _delay_us( TLC5925_DEF_DELAY );                                               // delay
    TLC5925_PORT_NOTOE = TLC5925_PORT_NOTOE | TLC5925_PIN_NOTOE_VALUE ;           // !OE = 1 
    _delay_us( TLC5925_DEF_DELAY );                                               // delay
    for ( i = 0 ; i <= 15 ; i++ ){                                                // loop--begin      
      if ( this->ledStates [ 15 - i ] == 0 ){
        TLC5925_PORT_SDI = TLC5925_PORT_SDI & ( 255 - TLC5925_PIN_SDI_VALUE );    //   set next bit
      }else{
        TLC5925_PORT_SDI = TLC5925_PORT_SDI | TLC5925_PIN_SDI_VALUE ;             //   set next bit
      }            
      _delay_us( TLC5925_DEF_DELAY );                                             //   delay
      TLC5925_PORT_CLK = TLC5925_PORT_CLK | TLC5925_PIN_CLK_VALUE ;               //   set CLK = 1  
      _delay_us( TLC5925_DEF_DELAY );                                             //   delay
      TLC5925_PORT_CLK = TLC5925_PORT_CLK & ( 255 - TLC5925_PIN_CLK_VALUE ) ;     //   set CLK = 0
      _delay_us( TLC5925_DEF_DELAY );                                             //   delay
    }                                                                             // loop--end
    TLC5925_PORT_LE = TLC5925_PORT_LE | TLC5925_PIN_LE_VALUE ;                    // set LE = 1
    _delay_us( TLC5925_DEF_DELAY );                                               // delay
    TLC5925_PORT_LE = TLC5925_PORT_LE & ( 255 - TLC5925_PIN_LE_VALUE ) ;          // set LE = 0
    _delay_us( TLC5925_DEF_DELAY );                                               // delay
    TLC5925_PORT_NOTOE = TLC5925_PORT_NOTOE & ( 255 - TLC5925_PIN_NOTOE_VALUE ) ; // !OE = 0
  }
  
  
};