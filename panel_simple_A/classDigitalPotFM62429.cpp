﻿#include <util/delay.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sterowanie układem FM62429 ( pot. cyfrowy )
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define VOL_CLK bit_1_value
#define PORT_VOL_CLK PORTD

#define VOL_DATA bit_1_value
#define PORT_VOL_DATA PORTA

class classDigitalPotFM62429{

  public:

    static const int8_t MAX_ATTENTUATION = 84 ;
    static const int8_t MUTE_ATTENTUATION = 84 ;

    void set ( uint8_t attLevel, uint8_t channel, uint8_t both ) {
      
      attLevel = this->MUTE_ATTENTUATION - attLevel ;      
      
      if ( both > 1 || channel > 1 ) return ;          // są tylko 2 kanały
      if ( attLevel > this->MAX_ATTENTUATION ) attLevel = this->MUTE_ATTENTUATION ;  // blokada na maksymalne tłumienie.

      uint16_t dataToSend = 0 ;                        // Dane / bity do wysłania
                                                       // Przygotowujemy 11 bitów zgodnie ze specyfikacją układu
      dataToSend |= channel ;                          // numer kanału
      dataToSend |= ( both << 1 );                     // 0 = oba kanały, 1 = każdy kanał osobno
      if ( attLevel <= this->MAX_ATTENTUATION ) {
        dataToSend |= ( 21 - ( attLevel / 4 ) ) << 2 ; // ATT1
        dataToSend |= ( 3 -  ( attLevel % 4 ) ) << 7 ; // ATT2
      }
      dataToSend |= 0b11 << 9;                         // ostatnie bity = 11

      //
      // wysyłamy dane
      for ( uint8_t i = 0; i <= 10; i++ ) {
        _delay_us( 3 );
        PORT_VOL_DATA &= ~VOL_DATA ;   // DATA = 0
        _delay_us( 3 );
        PORT_VOL_CLK &= ~VOL_CLK ;     // CLK = 0
        _delay_us( 3 );
        if ( ( ( dataToSend >> i ) & 0x01 ) == 1 ) {
          PORT_VOL_DATA |= VOL_DATA ;  // DATA = 1
          } else {
          PORT_VOL_DATA &= ~VOL_DATA ; // DATA = 0
        }
        _delay_us( 3 );
        PORT_VOL_CLK |= VOL_CLK ;      // CLK = 1
      }
      _delay_us( 3 );
      PORT_VOL_DATA |= VOL_DATA ;      // DATA = 1
      _delay_us( 3 );
      PORT_VOL_CLK &= ~VOL_CLK ;       // CLK = 0
      _delay_us( 3 );

      return;
    }

};

