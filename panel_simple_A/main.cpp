﻿/*
 * panel_simple_A_master.cpp
 * Created: 06.07.2020 03:39:29
 * Author : maciej.szczepanski
 *
 */

#define F_CPU 8000000


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <math.h>  

#include  "ampPanel.h"

#include  "classTools.cpp"
classTools Tools ;
#include "classDigitalPotFM62429.cpp"
classDigitalPotFM62429 DigiPot;
#include  "classTLC5925.cpp"
classTLC5925 LedBar ;
#include  "classEncoder.cpp"
classEncoder Encoder ;
#include "classInOutSelectors.cpp"
classInOutSelectors InOutSelectors;
#include  "classAmplifierDriver.cpp"
classAmplifierDriver Driver ;
#include  "classRemoteRC5.cpp"
classRemoteRC5 Remote ;
#include  "classMenu.cpp"
classMenu Menu ;

void init() {
  //
  // Konfiguracja wejść / wyjść uC
  // DDRx - GDY BIT USTAWIONY TO MAMY WYJŚCIE
  DDRA  = bit_0_value | bit_1_value ;
  PORTA = ~DDRA ;
  DDRB  = bit_5_value | bit_4_value | bit_3_value | bit_1_value | bit_0_value ; // bit 2 to encoderBtn, bit 6 to detekcja AC
  PORTB = ~DDRB ;
  DDRD  = bit_6_value | bit_4_value | bit_1_value | bit_0_value ; // bity 2, 3 to encoder reszta wyjścia, 5 to pilot
  PORTD = ~DDRD ;
  cli();
  
  //
  // Pobieram stan panelu zapisany w EEPROM i ustawiam wartości początkowe.  
  Driver.init();

  //
  // Inicjuję LedBar ( linię diod )
  LedBar.init() ;
  
  //
  // Inicjuję enkoder
  Encoder.init( 0, DigiPot.MUTE_ATTENTUATION, Driver.volume ) ;
  
  //
  // Inicjuję ustawienia Menu
  Menu.init();
  LedBar.shortDemo(3);
  
  //
  // Włączam głosniki
  int8_t tmp;
  tmp = Driver.volume ;
  Driver.setVolume ( 0 ) ;
  InOutSelectors.outputSet ( 1 );
  Driver.setVolumeFadeTo ( tmp , 25 );
  
  return;
}

//
// Przerwania obsługi enkodera
//
ISR ( INT0_vect ) {
  cli();
  Encoder.pinBintFunction();
}

ISR ( INT1_vect ) {
  cli();
  Encoder.pinAintFunction();
}

//
// MAIN PROGRAM
//
int main(void) {
  
  init();    
  
  while (1) {

    //
    // Jeśli nastąpiła zmiana pozycji enkodera ...
    if  ( Menu.encoderLastValue != Encoder.currentValue ) {
      Menu.encoderPositionChange() ;
    }
    //
    // Zmiany z enkodera obsłużone więc przywracam wywoływanie przerwań.
    sei();

    //
    // Jeśli wykryto początek transmisji z pilota
    if  ( Remote.isFrameBegin() ) {
      Menu.remoteCommandAnalyse();
    }

    //
    // Obsługa przycisku enkodera. Naciskanie przełącza stany Menu oraz odpowiada za przełączanie źródeł.
    if ( Encoder.isButtonPressed() ) {
      //
      // Krótki delay aby wyeliminować drgania styków.
      _delay_ms(50) ;
      Menu.buttonPressed();
    }

    //
    // Cykliczne odświeżam "ekran" użytkownika.
	// Nie muszę tego robić w każdym przebiegu pętli głównej.
	if ( ( Driver.stateToSave )&&( Menu.tmpCounter % 2048 == 0 ) ){
      Menu.render();
	}

    //
    // Cyklicznie i po wykryciu zmiany zapisuję stan do EEPROM
    // Nie wykonuję w każdym cyklu żeby np. podczas zwiększania głośności nie zapisywać po każdej zmianie.
    // W ten sposób ograniczam ilość zapisów do pamięci EEPROM.
    // Czyli zapisuję tylko po zmianie i nie częściej niż raz na kilka sekund.
    if ( ( Driver.stateToSave )&&( Menu.tmpCounter % 16384 == 0 ) ){
      Driver.saveToEEPROM() ;
    }
	
	//
	// Detekcja zaniku napięcia zasilania.
	// W przypadku wykrycia odłączam głośniki na kilka sekund.
	// Jeśli zasilanie AC powróci to włączam ponownie.
	if ( ( AC_DETECTION_PORT & AC_DETECTION_PIN_VALUE ) == 0 ){
	  Driver.noPowerAction();
	  Driver.stateToSave = true ;
	} else {
	  InOutSelectors.outputSet(1);
	}

    Menu.tmpCounter++;
  }
}



