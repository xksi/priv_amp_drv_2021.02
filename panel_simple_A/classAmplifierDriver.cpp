﻿#include <avr/eeprom.h>

class classAmplifierDriver{

  public:
  
    uint8_t input ;
    uint8_t volume ;
    int8_t  balance ; // -MUTE_ATTENTUATION .. 0 .. +MUTE_ATTENTUATION
    uint8_t stateXorSum ;
    uint8_t volBeforeMute ;
    uint8_t isMuted ;
  
    uint8_t remoteAddress ;
    uint8_t remoteCmdPower ;
    uint8_t remoteCmdVolumeUp ;
    uint8_t remoteCmdVolumeDn ;
    uint8_t remoteCmdMute ;
    uint8_t remoteCmdInputNext ;
    uint8_t remoteCmdInputPrev ;
    uint8_t remoteCmdBalanceLeft ;
    uint8_t remoteCmdBalanceRight ;
    uint8_t remoteCmdMenuSelect ;

    bool stateToSave ;

    void init(){
      this->loadFromEEPROM() ;
      InOutSelectors.inputSet( this->input );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FUNKCJE GŁOŚNOŚCI I BALANSU
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void setVolume( uint8_t value ){
      this->volume = value ;
      this->stateToSave = true ;            
      volatile uint16_t chLeft = value ;
      volatile uint16_t chRight = value ;      
      if ( this->balance == 0 ){
        if ( value > DigiPot.MUTE_ATTENTUATION ) value = DigiPot.MUTE_ATTENTUATION ;
        DigiPot.set ( value, 0, 0 ) ;
        return ;
      }      
      uint8_t tmpBal = ( value * 256 - value * ( ( Tools.numberAbs(this->balance) * 256 ) / DigiPot.MUTE_ATTENTUATION ) ) / 256 ;
      if ( this->balance < 0 ){
        chLeft = tmpBal ;
         } else {
         chRight = tmpBal ;
      }
      DigiPot.set ( (uint8_t)chLeft, 0, 1 ) ;
      DigiPot.set ( (uint8_t)chRight, 1, 1 ) ;
      return ;      
    }
    
    void setVolumeFadeTo( uint8_t newValue, uint8_t delayTime ){      	  
	  if ( newValue == this->volume ) return ;	  
	  int8_t i ;
      if ( newValue > this->volume ){
        for ( i = this->volume ; i <= newValue ; i++ ){
          this->setVolume( i );
          Tools.delay( delayTime );
          LedBar.showNiceExp ( i );     
        }
      }else{
        for ( i = this->volume ; i >= newValue ; i-- ){
          this->setVolume( i );
          Tools.delay( delayTime );
          LedBar.showNiceExp ( i );
        }        
      }
      return;
    }
    
    void setVolumeDiff ( int8_t value ){
      int8_t newVolume = this->volume + value ;
      if ( newVolume > DigiPot.MUTE_ATTENTUATION ) newVolume = DigiPot.MUTE_ATTENTUATION ;
      if ( newVolume < 0 ) newVolume = 0 ;
      this->setVolume( newVolume );
      return;
    }    
    
    void setBalance ( int8_t value ){
      this->balance = value ;
      if ( this->balance > DigiPot.MUTE_ATTENTUATION ) this->balance = DigiPot.MUTE_ATTENTUATION;
      if ( this->balance < ( DigiPot.MUTE_ATTENTUATION * -1 ) ) this->balance = ( DigiPot.MUTE_ATTENTUATION * -1 );
      this->setVolume( this->volume );
      this->stateToSave = true ;
      return;
    }

    void setBalanceDiff ( int8_t value ){
      int8_t newBalance = this->balance + value ;
      if ( newBalance > DigiPot.MUTE_ATTENTUATION ) newBalance = DigiPot.MUTE_ATTENTUATION ;
      if ( newBalance < -DigiPot.MUTE_ATTENTUATION ) newBalance = -DigiPot.MUTE_ATTENTUATION ;
      this->setBalance( newBalance );
      return;
    }

    void muteToggle(){
      if ( this->isMuted == 1 ){
        this->volume = 0 ;
        this->setVolumeFadeTo ( this->volBeforeMute, 2 ) ;
        this->isMuted = 0 ;
      } else {
        this->volBeforeMute = this->volume ;
        this->setVolumeFadeTo ( 0, 2 ) ;
        this->isMuted = 1 ;
      }
      return ;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FUNKCJE SELEKTORA WEJŚC
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void setInputNext(){
      this->stateToSave = true ;
      if ( this->input == 1 ){
        this->input = 0 ;
      } else {
        this->input = 1 ;        
      }
	  uint8_t volBefMute = this->volume ;
	  this->setVolumeFadeTo ( 0, 0 ) ;
      InOutSelectors.inputSet( this->input );
	  this->setVolumeFadeTo ( volBefMute, 0 ) ;
      return;
    }
    
    void setInputPrev(){
      this->setInputNext();
      return;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FUNKCJE SELEKTORA WYJŚC
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void noPowerAction(){
	  _delay_ms( 50 );
	  InOutSelectors.outputSet ( 0 );
	  LedBar.setAllOff();
	  for ( uint8_t i = 0 ; i < 20 ; i++ ){
	    _delay_ms( 250 );
		LedBar.set( 1, LedBar.LED_STATE_INVERSE );
		LedBar.set( LedBar.LAST_LED_NO, LedBar.LED_STATE_INVERSE );
	  }

	}


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FUNKCJE PAMIĘCI EEPROM
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    uint8_t calcStateXorSum(){
      this->stateXorSum  = this->input ^ this->volume ^ this->balance ;
      this->stateXorSum ^= this->remoteAddress ^ this->remoteCmdPower ^ this->remoteCmdVolumeUp ^ this->remoteCmdVolumeDn ;
      this->stateXorSum ^= this->remoteCmdMute ^ this->remoteCmdInputNext ^ this->remoteCmdInputPrev ;
      this->stateXorSum ^= this->volBeforeMute ^ this->isMuted ^ this->remoteCmdBalanceLeft ^ this->remoteCmdBalanceRight ^ 23 ;
      return this->stateXorSum ;
    }

    void loadFromEEPROM() {
      this->input                = eeprom_read_byte( (uint8_t*)21 ) ;
      this->volume               = eeprom_read_byte( (uint8_t*)22 ) ;
      this->balance              = eeprom_read_byte( (uint8_t*)23 ) ;
      this->remoteAddress        = eeprom_read_byte( (uint8_t*)24 ) ;
      this->remoteCmdPower       = eeprom_read_byte( (uint8_t*)25 ) ;
      this->remoteCmdVolumeUp    = eeprom_read_byte( (uint8_t*)26 ) ;
      this->remoteCmdVolumeDn    = eeprom_read_byte( (uint8_t*)27 ) ;
      this->remoteCmdMute        = eeprom_read_byte( (uint8_t*)28 ) ;
      this->remoteCmdInputNext   = eeprom_read_byte( (uint8_t*)29 ) ;
      this->remoteCmdInputPrev   = eeprom_read_byte( (uint8_t*)30 ) ;
      this->remoteCmdBalanceLeft = eeprom_read_byte( (uint8_t*)31 ) ;
      this->remoteCmdBalanceRight= eeprom_read_byte( (uint8_t*)32 ) ;
      this->remoteCmdMenuSelect  = eeprom_read_byte( (uint8_t*)33 ) ;
      this->volBeforeMute        = eeprom_read_byte( (uint8_t*)34 ) ;
      this->isMuted              = eeprom_read_byte( (uint8_t*)35 ) ;

      uint8_t checkSum = eeprom_read_byte( (uint8_t*)10 ) ;
      this->calcStateXorSum();

      //
      // Weryfikuję sumę kontrolną z danych aby w przypadku nowego układu
      // lub awarii EEPROM załadować dane domyślne.
      if ( checkSum != this->stateXorSum ) {
        //
        // Ustawiam wartości domyślne
        this->input                 = 1 ;
        this->volume                = 20 ;
        this->balance               = 0 ;
        this->remoteAddress         = 0x00 ; // kod RC5
        this->remoteCmdPower        = 0x0C ; // kod RC5
        this->remoteCmdVolumeUp     = 0x10 ; // kod RC5
        this->remoteCmdVolumeDn     = 0x11 ; // kod RC5
        this->remoteCmdMute         = 0x0D ; // kod RC5
        this->remoteCmdInputNext    = 0x20 ; // kod RC5
        this->remoteCmdInputPrev    = 0x21 ; // kod RC5
        this->remoteCmdBalanceLeft  = 0x01 ; // kod RC5
        this->remoteCmdBalanceRight = 0x03 ; // kod RC5
        this->remoteCmdMenuSelect   = 0x12 ; // kod RC5
        this->volBeforeMute         = 20 ;
        this->isMuted               = 0 ;
      }
      return;
    }

    void saveToEEPROM() {
      this->calcStateXorSum();
      eeprom_write_byte( (uint8_t*)10, this->stateXorSum ) ;

      //
      // Zapisuję stan sterownika.
      eeprom_write_byte( (uint8_t*)21, this->input ) ;
      eeprom_write_byte( (uint8_t*)22, this->volume ) ;
      eeprom_write_byte( (uint8_t*)23, this->balance ) ;
      eeprom_write_byte( (uint8_t*)24, this->remoteAddress ) ;
      eeprom_write_byte( (uint8_t*)25, this->remoteCmdPower ) ;
      eeprom_write_byte( (uint8_t*)26, this->remoteCmdVolumeUp ) ;
      eeprom_write_byte( (uint8_t*)27, this->remoteCmdVolumeDn ) ;
      eeprom_write_byte( (uint8_t*)28, this->remoteCmdMute ) ;
      eeprom_write_byte( (uint8_t*)29, this->remoteCmdInputNext ) ;
      eeprom_write_byte( (uint8_t*)30, this->remoteCmdInputPrev ) ;
      eeprom_write_byte( (uint8_t*)31, this->remoteCmdBalanceLeft ) ;
      eeprom_write_byte( (uint8_t*)32, this->remoteCmdBalanceRight ) ;
      eeprom_write_byte( (uint8_t*)33, this->remoteCmdMenuSelect ) ;
      eeprom_write_byte( (uint8_t*)34, this->volBeforeMute ) ;
      eeprom_write_byte( (uint8_t*)35, this->isMuted ) ;

      this->stateToSave = false ;
      return;
    }

};