﻿class classMenu{
    
  public:

    static const uint8_t MENU_VOLUME = 1 ;
    static const uint8_t MENU_BALANCE = 2 ;
    static const uint8_t ENC_BTN_LONG_PRESS_TIME = 100 ; // 100 = 1 sec.

    uint8_t  state ;
    int8_t   encoderLastValue ;
    uint8_t  remoteLastCommand ;
    uint8_t  remoteLastToggle ;
    uint16_t tmpCounter ;

    void init(){
      this->encoderLastValue = Encoder.currentValue ;
      this->remoteLastCommand = 0 ;
      this->remoteLastToggle = 0 ;
      this->tmpCounter = 0 ;
      this->state = this->MENU_VOLUME ;
    }
	
    void setMenu( uint8_t menuId )
    {
	  Driver.stateToSave = true ;
      if ( menuId == this->MENU_BALANCE ){
        this->state = this->MENU_BALANCE ;
        Encoder.minValue = DigiPot.MUTE_ATTENTUATION * -1 ;
        Encoder.maxValue = DigiPot.MUTE_ATTENTUATION ;
        Encoder.currentValue = Driver.balance ;
      }
      if ( menuId == this->MENU_VOLUME ){
        this->state = this->MENU_VOLUME ;
        Encoder.minValue = 0 ;
        Encoder.maxValue = DigiPot.MUTE_ATTENTUATION ;
        Encoder.currentValue = Driver.volume ;
      }
      return;
    }
    
    void render(){
      //
      // Gdy w trybie ustawiania głośności.
      if ( ( this->state == this->MENU_VOLUME )&&( Driver.isMuted == 0 ) ) {
		LedBar.showNiceExp ( Driver.volume );
      }

      //
      // Gdy w trybie ustawiania balansu.
      if ( ( this->state == this->MENU_BALANCE )&&( Driver.isMuted == 0 ) ) {
		LedBar.showNiceBalance ( Driver.balance );  
      }

      //
      // Gdy w trybie MUTE.
      if ( Driver.isMuted == 1 ) {
        LedBar.setAllOff() ;
        LedBar.set ( 1, LedBar.LED_STATE_ON ) ;
        LedBar.set ( LedBar.LAST_LED_NO, LedBar.LED_STATE_ON ) ;
      }
      return;
    }

    void encoderPositionChange(){
      
      // i jesteśmy w MENU_VOLUME to znaczy, że zmieniono głośność.
      if ( this->state == this->MENU_VOLUME ){
        if ( Driver.isMuted == 1 ) Driver.muteToggle() ;
		Driver.setVolume ( Encoder.currentValue ) ;		
      }

      // i jesteśmy w MENU_BALANCE to znaczy, że zmieniono balans.
      if ( this->state == this->MENU_BALANCE ){
		if ( Driver.isMuted == 1 ) Driver.muteToggle() ;
        Driver.setBalance ( Encoder.currentValue ) ;
      }

      this->encoderLastValue = Encoder.currentValue ;
      
      return;
    }

    void remoteCommandAnalyse(){
      //
      // Odbieram ramkę z pilota.
      Remote.readFrame();

      //
      // Sprawdzam czy ramka poprawna i ma dobry adres.
      if ( ( Remote.value != Remote.DECODE_ERROR )&&( Remote.address == Driver.remoteAddress ) ){

        //
        // Rozróżniam 2 typy komend jednorazowe ( bit toggle nie ma znaczenia ) i ciągłe ( np. volume gdzie przytrzymanie przycisku daje efekt )
        //
        // Najpierw komendy bez powtórzeń:
        if ( ( Remote.command != this->remoteLastCommand )||( Remote.toggle != this->remoteLastToggle ) ) {
          if ( Remote.command == Driver.remoteCmdVolumeUp ) { if ( Driver.isMuted == 1 ) Driver.muteToggle() ; Driver.setVolumeDiff (  1 ) ; this->setMenu ( this->MENU_VOLUME ) ; }
          if ( Remote.command == Driver.remoteCmdVolumeDn ) { if ( Driver.isMuted == 1 ) Driver.muteToggle() ; Driver.setVolumeDiff ( -1 ) ; this->setMenu ( this->MENU_VOLUME ) ; }
          if ( Remote.command == Driver.remoteCmdBalanceLeft ) { if ( Driver.isMuted == 1 ) Driver.muteToggle() ; Driver.setBalanceDiff ( -1 ) ; this->setMenu ( this->MENU_BALANCE ) ; }
          if ( Remote.command == Driver.remoteCmdBalanceRight ) { if ( Driver.isMuted == 1 ) Driver.muteToggle() ; Driver.setBalanceDiff (  1 ) ; this->setMenu ( this->MENU_BALANCE ) ; }
          if ( Remote.command == Driver.remoteCmdInputNext ) Driver.setInputNext () ;
          if ( Remote.command == Driver.remoteCmdInputPrev ) Driver.setInputPrev () ;
          if ( Remote.command == Driver.remoteCmdMute ) Driver.muteToggle() ;
          // if ( Remote.command == Driver.remoteCmdPower ) Driver.powerToggle() ;
          if ( Remote.command == Driver.remoteCmdMenuSelect ) {
            if ( this->state == this->MENU_VOLUME ){ this->setMenu ( this->MENU_BALANCE ) ; } else { this->setMenu ( this->MENU_VOLUME ) ; }
          }
        }

        //
        // Teraz z powtórzeniami
        if ( ( Remote.command == this->remoteLastCommand )&&( Remote.toggle == this->remoteLastToggle ) ) {
          if ( Remote.command == Driver.remoteCmdVolumeUp ){ Driver.setVolumeDiff ( 3 ) ; this->setMenu ( this->MENU_VOLUME ); }
          if ( Remote.command == Driver.remoteCmdVolumeDn ){ Driver.setVolumeDiff ( -3 ) ; this->setMenu ( this->MENU_VOLUME ); }
          if ( Remote.command == Driver.remoteCmdBalanceLeft ) { Driver.setBalanceDiff ( -3 ) ; this->setMenu ( this->MENU_BALANCE ); }
          if ( Remote.command == Driver.remoteCmdBalanceRight ) { Driver.setBalanceDiff (  3 ) ; this->setMenu ( this->MENU_BALANCE ); }
        }
      }

      this->remoteLastToggle = Remote.toggle ;
      this->remoteLastCommand = Remote.command ;
      return;
    }

    void buttonPressed(){

      //
      // Wykrywam długie naciśnięcie przycisku.
      uint16_t btnPressedTime = 0 ; // 100 = ~1 sekunda
      while ( ( Encoder.isButtonPressed() ) && ( btnPressedTime < this->ENC_BTN_LONG_PRESS_TIME ) ){
        _delay_ms(10);
        btnPressedTime ++;
      }

      //
      // Gdy długie naciśnięcie przycisku ...
      if ( btnPressedTime >= this->ENC_BTN_LONG_PRESS_TIME  ){
        this->setMenu ( this->MENU_BALANCE );
        this->encoderLastValue = Encoder.currentValue ;
      }

      //
      // Gdy krótkie naciśnięcie przycisku ...
      if ( btnPressedTime < this->ENC_BTN_LONG_PRESS_TIME  ){

        if ( this->state == this->MENU_VOLUME ) {
          //
          // Przełącz wejścia.
          Driver.setInputNext() ;
        }

        //
        // Powrót do menu Volume.
        if ( this->state == this->MENU_BALANCE ) {
          this->setMenu ( this->MENU_VOLUME ) ;
          this->encoderLastValue = Encoder.currentValue ;
        }
      }

      //
      // Czekam aż przycisk zostanie puszczony i sygnalizuję miganiem.
      //if ( Encoder.isButtonPressed() ) LedBar.setRange ( 0, LedBar.LAST_LED_NO, LedBar.LED_STATE_OFF ) ;
      while ( Encoder.isButtonPressed() ){
        LedBar.set ( LedBar.LAST_LED_NO, LedBar.LED_STATE_INVERSE ) ;
        _delay_ms(70);
      }

      return;
    }

};