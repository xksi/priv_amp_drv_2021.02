﻿/*
  Definicje wartości bitów
*/

#define bit_0_value 1
#define bit_1_value 2
#define bit_2_value 4
#define bit_3_value 8
#define bit_4_value 16
#define bit_5_value 32
#define bit_6_value 64
#define bit_7_value 128


#define AC_DETECTION_PORT           PINB
#define AC_DETECTION_PIN_VALUE      bit_6_value
