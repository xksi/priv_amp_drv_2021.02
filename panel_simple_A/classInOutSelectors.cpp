#define INOUTSEL_PORT_IN            PORTD
#define INOUTSEL_PIN_IN_VALUE       bit_0_value
#define INOUTSEL_PORT_OUT           PORTA
#define INOUTSEL_PIN_OUT_VALUE      bit_0_value

#define INOUTSEL_PORT_LED_IN1       PORTB
#define INOUTSEL_PIN_LED_IN1_VALUE  bit_4_value
#define INOUTSEL_PORT_LED_IN2       PORTB
#define INOUTSEL_PIN_LED_IN2_VALUE  bit_5_value

class classInOutSelectors{

  public:

    static const uint8_t FADE_SPEED_FAST =  3 ;
    static const uint8_t FADE_SPEED_SLOW = 50 ;
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Sterowanie pinami input, output
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void inputSet ( uint8_t no ) {
      if ( no == 1 ){
        INOUTSEL_PORT_IN = INOUTSEL_PORT_IN | INOUTSEL_PIN_IN_VALUE ;
        INOUTSEL_PORT_LED_IN1 = INOUTSEL_PORT_LED_IN1 | INOUTSEL_PIN_LED_IN1_VALUE ;
        INOUTSEL_PORT_LED_IN2 = INOUTSEL_PORT_LED_IN2 & ( 255 - INOUTSEL_PIN_LED_IN2_VALUE ) ;
      } else {
        INOUTSEL_PORT_IN = INOUTSEL_PORT_IN & ( 255 - INOUTSEL_PIN_IN_VALUE  ) ;
        INOUTSEL_PORT_LED_IN2 = INOUTSEL_PORT_LED_IN2 | INOUTSEL_PIN_LED_IN2_VALUE ;
        INOUTSEL_PORT_LED_IN1 = INOUTSEL_PORT_LED_IN1 & ( 255 - INOUTSEL_PIN_LED_IN1_VALUE ) ;
      }            
      return ;
    }

    void outputSet ( uint8_t no ) {
      if ( no == 1 ){
        INOUTSEL_PORT_OUT = INOUTSEL_PORT_OUT | INOUTSEL_PIN_OUT_VALUE ;
        } else {
        INOUTSEL_PORT_OUT = INOUTSEL_PORT_OUT & ( 255 - INOUTSEL_PIN_OUT_VALUE ) ;
      }      
      return ;
    }

};