  /* 
  
    Konfiguracja i znaczenie pinów uC ( atTiny4313 )
  
    PIN20         - VCC
    PIN19 [ PB7 ] - SCK                        / programator
    PIN18 [ PB6 ] - IN  / MISO : AC Detection  / programator
    PIN17 [ PB5 ] - OUT : LED SEL2             / programator
    PIN16 [ PB4 ] - OUT : LED SEL1
    PIN15 [ PB3 ] - OUT : TO TLC5925  !OE (21)
    PIN14 [ PB2 ] - IN  : Encoder BTN
    PIN13 [ PB1 ] - OUT : TO TLC5925  LE  (4)
    PIN12 [ PB0 ] - OUT : TO TLC5925  CLK (3)
    PIN11 [ PD6 ] - OUT : TO TLC5925  SDI (2)
    PIN10         - GND
    PIN9  [ PD5 ] - IN  : pilot
    PIN8  [ PD4 ] - OUT : LED PWR
    PIN7  [ PD3 ] - IN  : Encoder
    PIN6  [ PD2 ] - IN  : Encoder
    PIN5  [ PA0 ] - OUT : do sterownika Speaker Selector/On/Off
    PIN4  [ PA1 ] - OUT : do sterownika Digital potentiometer SDA
    PIN3  [ PD1 ] - OUT : do sterownika Digital potentiometer SCL
    PIN2  [ PD0 ] - OUT : do sterownika Input Selector
    PIN1  [ PA2 ] - RST :                      / programator
  */
